#!/usr/bin/python3

# yaml file in $install_dir/dataset
# debian directories in $install_dir/packages


# we need 
import yaml
import sys
import os
import shutil
import subprocess
import glob
import debian.deb822
import textwrap
import debian.changelog
from datetime import datetime
import pytz
import re
from debian.changelog import Changelog, Version


# read the packages to work on
stream_packages_list = open('dataset/packages_osrf_rosworld.yml', 'r')
stream_packages_relations = open('dataset/relations_osrf_debian.yml', 'r')

packages_list = yaml.safe_load(stream_packages_list)
packages_relations = yaml.safe_load(stream_packages_relations)

ros4debian_dir = os.getcwd()
# Function that do:
# 
# creates a directory with its debian name in $install_dir/packages
# download package from sources osrf  
# move the necessary files to keeps _only_ 
# sources + new_debian_dir + debian.ubuntu

def restore_files(package, debian_package):
 
  # Parent Directory path

  current_dir = os.getcwd()
  # We work in a directory called packages from the current dir
  #root_path = os.path.join(current_dir, 'packages')
  #os.makedirs(root_path, exist_ok = True)
  #os.chdir(root_path)
  root_path = os.getcwd()

  package = package.rstrip()
  path = os.path.join(root_path, debian_package)

  os.makedirs(path, exist_ok = True)
  # Changing the CWD
  os.chdir(path)
  
  orig = glob.glob(f'{package}*orig*')
    
  if not (len(orig) == 0): # string
    source_orig = orig[0]
    
  suffix_idx = source_orig.find('.orig')
  orig_part = source_orig[suffix_idx:]
  version_str = source_orig.replace(package +'_','')
  version_str = version_str.replace(orig_part, '')
    
  print("Package is: ",package)
  print("Orig_part is: ",orig_part)
  print("Version_part is: ",version_str)
    
  os.rename(source_orig,debian_package + '_' + version_str + orig_part)

  # remove package directory if exists
  # before rename
  pathp = os.path.join(path, debian_package)
  shutil.rmtree(pathp, ignore_errors=True)
  os.rename(package + '-' + version_str, debian_package)
  
  # now copy the debian directory
  for ubuntu_files in glob.glob(f'*focal*'):
    os.remove(ubuntu_files)
  
  # remove debian directory
  pathd = os.path.join(path, debian_package + '/debian')

  shutil.rmtree(pathd, ignore_errors=True)
  os.makedirs(pathd, exist_ok = True)
  
  shutil.move('control', pathd + '/control')
  shutil.move('changelog', pathd + '/changelog')
  shutil.move('rules', pathd + '/rules')
  os.chmod(pathd + '/rules', 0o755)

  os.makedirs(pathd + '/source', exist_ok = True)
  shutil.copy(ros4debian_dir + '/templates/format',pathd + '/source/format')
  os.chdir(current_dir)

# end_def



# function that return a list with name and version if exists
def split_pkg_version(package):
  ver = re.findall(r'\(.*?\)', package) # there's only one
  
  if len(ver)==0:
    pkg_version = [package,'']
  else:
    version = ver[0]
    name = package.replace(version, '')
    name = name.replace(' ','')
    pkg_version = [name,version]
  return pkg_version

# Creates changelog for a package with a new entry of ros4debian

def create_changelog(debian_package):

    UTC = pytz.utc
    datetime_utc = datetime.now(UTC)
    time = datetime_utc.strftime('%a, %d %b %Y %H:%M:%S %z')
    
    current_dir = os.getcwd()
    # We work in a directory called packages from the current dir
    root_path = os.path.join(current_dir, 'packages')
    os.chdir(root_path)
    
    # Import from the osrf changelog file
    with open(root_path + '/' + debian_package +'/debian/changelog') as f:
        line = f.readline().strip()
        f.close()

    version = re.findall(r'\((.*?) *\)', line)
    ptime = datetime_utc.strftime('%Y%m%d')

    # drop focal string
    # we use it because it is more equivalent to bullseye
    version = version[0].replace('focal','')
    version = version + '~r4d+' + ptime
    changelog = Changelog()

    changelog.new_block(package=debian_package,
                        version=Version(version),
                        distributions='unstable',
                        urgency='low',
                        author='Ros4Debian Autogenerated <ros4debian@example.com>',
                        date=time,
                        )

    changelog.add_change('');
    changelog.add_change('  * Version rebuild for Debian from ros4debian');
    changelog.add_change('');


    new_changelog = open(root_path + '/' + debian_package +'/changelog','w')
    old_changelog = open(root_path + '/' + debian_package +'/debian/changelog', 'r')

    lines_old_changelog = old_changelog.readlines()
    new_changelog.write(str(changelog))
    for line in lines_old_changelog: # write old content after new
        new_changelog.write(line)
    new_changelog.close()
    old_changelog.close()


# Function that from a control file creates a create_rules
# file from a template, with python code or not

def create_rules(debian_package):

  current_dir = os.getcwd()
  # We work in a directory called packages from the current dir
  root_path = os.path.join(current_dir, 'packages')
  os.chdir(root_path)

  debian_package = debian_package.rstrip()

  path = os.path.join(root_path, debian_package)

  # Changing the CWD
  os.chdir(path)
  for paragraph in debian.deb822.Deb822.iter_paragraphs(open('control')):
    for field, data in paragraph.items():
      if field == 'Build-Depends':
        ndata = convert_to_package_list(data)
        for dep in ndata: # checking the list
          if(dep.startswith('python')): # we generate python code
            shutil.copy(current_dir + '/' + 'templates/rules.python', 'rules')
          else:
            shutil.copy(current_dir + '/' + 'templates/rules.default', 'rules')
            
      
  # Now we must modified the file
  os.chdir(current_dir)




## 
# Function that from a string of packages
# returns a list of items of packages

def convert_to_package_list(data):
  data = data.split(",")
  new_data = []
  for item in data:
    if(item.startswith(' ')): # remove first if is ' ' 
      new_data.append(item[1:])
    else:
      new_data.append(item)
    #endif
  #endfor
  return new_data
#enddef

#
# Function that from a build_list modify it to a debian compatible new one
def modify_build_depends(build_list):
  nbuild_list = ['debhelper-compat (= 13)','dh-sequence-python3','libgtest-dev', 'libgmock-dev']
  for b_dep in build_list:
    package  = split_pkg_version(b_dep)# [0] name, [1] version
    package_name = str(package[0])
    package_version = str(package[1])
    
    #print("Package name and version: ", package_name, " " , package_version)
    
    if not package_name.startswith('debhelper'):
      if package_name in packages_list:
        if package_version:
          nbuild_list.append(packages_list[package_name][0] + ' ' + package_version)
        else:
          nbuild_list.append(packages_list[package_name][0])
      elif package_name in packages_relations:
        for item in packages_relations[package_name]['build']:
          if package_version:
            nbuild_list.append(item + ' ' + package_version)
          else:
            nbuild_list.append(item)
      else:
        if package_version:
          nbuild_list.append(package_name + ' ' + package_version)
        else:
          nbuild_list.append(package_name)
        #endif
      #endif
    #endif
  #endfor
  return nbuild_list
#enddef    

# Function that from a run_list modify it to a debian compatible new one
def modify_run_depends(run_list):
  nrun_list = ['${python3:Depends}']
  for r_dep in run_list:
    package  = split_pkg_version(r_dep)# [0] name, [1] version
    package_name = str(package[0])
    package_version = str(package[1])
    
    #print(package_name)
    
    if package_name in packages_list:
      if package_version:
         nrun_list.append(packages_list[package_name][0] + ' ' + package_version)
      else:
         nrun_list.append(packages_list[package_name][0])
      #endif
    elif package_name in packages_relations:
      if packages_relations[package_name]['run']:
        for item in packages_relations[package_name]['run']:
          if package_version:
            nrun_list.append(item + ' ' + package_version)
          else:
              nrun_list.append(item)
      #endif
        #endfor
    else:
      if package_version:
         nrun_list.append(package_name + ' ' + package_version)
      else:
         nrun_list.append(package_name)
        #endif
      #endif
  #endfor
  return nrun_list
#enddef    



# Function that:
# from a osrf d/control file from osrf creates
# a new one with its debian equivalences
def transform_control(package, debian_package):
  # Parent Directory path

  current_dir = os.getcwd()
  # We work in a directory called packages from the current dir
  root_path = os.path.join(current_dir, 'packages')
  os.chdir(root_path)

  package = package.rstrip()

  path = os.path.join(root_path, debian_package)

  # Changing the CWD
  os.chdir(path)
  new_control = open("control",mode="w",encoding="utf-8")

  for paragraph in debian.deb822.Deb822.iter_paragraphs(open('debian/control')):
    for field, data in paragraph.items():
      if field == 'Source':
        new_control.write("Source: " + debian_package + "\n")
      elif field == 'Build-Depends':
        ndata = convert_to_package_list(data)
        bbuild_depends = modify_build_depends(ndata)
        new_control.write("Build-Depends: ")
        for b_dep in bbuild_depends:
          new_control.write(b_dep)
          if not b_dep == bbuild_depends[-1]: # last one
            new_control.write(", ")
          else:
            new_control.write("\n")
      elif field == 'Standards-Version':
        new_control.write("Standards-Version: " + '4.6.0' + "\n")
      elif field == 'Package':
        new_control.write("\n" + "Package: " + debian_package + "\n")
      elif field == 'Depends':
        rdata = convert_to_package_list(data)
        run_depends = modify_run_depends(rdata)
        new_control.write("Depends: ")
        for r_dep in run_depends:
          new_control.write(r_dep)
          if not r_dep == run_depends[-1]: # last one
            new_control.write(", ")
          else:
            new_control.write("\n")
      elif field == 'Description':
        short_desc = data.split("\n")[0] # first line
        if isinstance(data, list):
          long_desc = data.split("\n")[1]
        elif isinstance(data, str):
          long_desc = data.split("\n")[0]
        
        long_desc_lines = textwrap.wrap(long_desc, width=78, break_long_words=False)
        new_control.write("Description: " + short_desc + "\n")
        for line_dl in long_desc_lines:
          if(line_dl[0] == ' '): # remove first if is ' ' 
            new_control.write(" " + line_dl[1:] + "\n")
          else:
            new_control.write(" " + line_dl + "\n")
      else:
        new_control.write(field + ": " + data + "\n")
    
  # Now we must modified the file
  os.chdir(current_dir)

# end_def




# Function that do:
# 
# creates a directory with its debian name in $install_dir/packages
# download package from sources osrf  
# keeps _only_ debian directory and drop the rest

def download_package(package, debian_package):
 
  # Parent Directory path

  current_dir = os.getcwd()
  # We work in a directory called packages from the current dir
  root_path = os.path.join(current_dir, 'packages')
  os.makedirs(root_path, exist_ok = True)
  os.chdir(root_path)

  package = package.rstrip()
  print ("Working with the package: ",package)

  path = os.path.join(root_path, debian_package)

  os.makedirs(path, exist_ok = True)
  # Changing the CWD
  os.chdir(path)
  subprocess.run(['apt-get', 'source', package])
  
  # now copy the debian directory
  for debian_dir in glob.glob(f'{package}*/debian'):
    #print("Package is",debian_dir)
    pathd = os.path.join(path,'debian')
    os.makedirs(pathd, exist_ok = True)           
    shutil.copytree(debian_dir, pathd, dirs_exist_ok=True)
            
    # now deleting files
    #del_files = glob.glob(f'{package}*')
    #path_tod = os.path.join(path,'to_delete')
    
    # delete previous
    #shutil.rmtree(path_tod, ignore_errors=True)
    #os.makedirs(path_tod, exist_ok = True)
    #for dfiles in del_files:
    #  shutil.move(dfiles, path_tod)
    
    #shutil.rmtree(path_tod, ignore_errors=True)
    
  # Now we must modified the file
  os.chdir(current_dir)

# end_def


package_to_work = sys.argv[1]

if package_to_work in packages_list:
    print("Package is in list and its debian name is:", packages_list[package_to_work][0])
    download_package(package_to_work, packages_list[package_to_work][0]) 
    transform_control(package_to_work, packages_list[package_to_work][0])
    create_rules(packages_list[package_to_work][0])
    create_changelog(packages_list[package_to_work][0])
    restore_files(package_to_work, packages_list[package_to_work][0])
elif package_to_work in packages_relations:
    print("Package is in Debian archive. Just install to build:")
    for item in packages_relations[package_to_work]['build']:
         print(item)
else:        
    sys.exit(package_to_work + " is NOT in list and cannot be processed")

    
